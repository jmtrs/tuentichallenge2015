/* Problem 2 - Almost Prime
 * Author: jmtrs
 * Languaje: GoLang
 * Execution: go run AlmostPrime.go < a.in > a.out
 */

package main

import "fmt"

func who(almPrime int) bool{
    
    j := 0
    
    for i :=2; j <=2 && i*i <= almPrime ; i++{
            
        for almPrime%i == 0 && j < 2 {
                almPrime /= i
                j++
        } 
    }
   if almPrime > 1{
        j++
    }
    return j == 2
}


func main(){
    
    var( aux int
         almost int
         prime int
         cont int )
    
	fmt.Scanf("%d", &aux)
	
	for i := 1; i <= aux; i++ {
	    fmt.Scanf("%d %d", &almost, &prime)
	    
	    for j := almost; j <= prime; j++ {
	        
    	    if who(j) {
    	       cont ++
    	       almost ++
    	    }
	    }
	    fmt.Printf("%d\n", cont)
	    cont = 0
	}	
}

