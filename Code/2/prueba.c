#include <stdio.h>

int who(int Aprime){
	int p, f = 0;
	
	for (p = 2; f < 2 && p*p <= Aprime; p++){
		while (Aprime % p == 0 && f < 2)
			Aprime /= p, f++;
	}
 
	if(Aprime >1){
		f++;
	}
	return f == 2;
}
 
int main(void)
{
	int i, j, almost, prime, aux, cont;
	
	scanf("%d", &aux);
	
	for (j = 1; j <= aux; j++) {
		
		scanf("%d %d", &almost, &prime);
 		
 		for (i = almost; i <= prime; i++) {
			
			if (who(i)) {
				cont ++;
				almost++;
			}
			
 		}
 		printf("%d\n", cont);
 		cont = 0;
	}
	return 0;
}