/* Problem 1 - The Buffer
 * Author: jmtrs
 * Languaje: GoLang
 * Execution: go run TheBuffer.go < a.in > a.out
 */

package main

import "fmt"
import "math"

func leave(site float64){
   
   if math.Mod(site, 2) == 1{
        fmt.Printf("%.f\n", (site/2) + 0.5)    
   } else {
   fmt.Printf("%.f\n", site / 2) 
   }
}


func main() {
    
    var aux int
    var site float64
    
	fmt.Scanf("%d", &aux)
	for i := 1; i <= aux; i++ {
	    fmt.Scanf("%f", &site) 
	    leave(site)
	}
	
}