package main
 
import (
	"bufio"
	//"errors"
	"fmt"
	//"io"
	"os"
)
 
func main() {
	if line, err := rsl("mininumbers.txt", 4); err == nil {
		fmt.Println("7th line:")
		fmt.Println(line)

	}
}
 
func rsl(fn string, n int) (string, error) {
	
	f, err := os.Open(fn)
	if err != nil {
		return "", err
	}
	defer f.Close()
	bf := bufio.NewReader(f)
	var line string
	for lnum := 0; lnum < n; lnum++ {
		line, err = bf.ReadString('\n')
		
	}
	return line, nil
}