 /* FIND OUT MAXIMUM TIME REPEATING NUMBER IN THE RANGE OF 0 TO N-1 */

#include<stdio.h>
#include <cstdlib> 
//#include<conio.h>

int main()
{
system("clear");
int a[20],n,i,max;
printf("\n Enter the size of Array :");
scanf("%d",&n);
printf("\n Enter the array elements in the range 0 to n-1 :");
for(i=0;i<n;i++)
scanf("%d",&a[i]);
for(i=0;i<n;i++)
	a[a[i]%n]+=n;

max=a[0]/n;
for(i=1;i<n;i++)
	if(a[i]/n>max)
		max=a[i]/n;

printf("\n Maximum repeating Elements : ");
for(i=0;i<n;i++)
 if(a[i]/n==max)
 printf(" %d ",i);

/* reconstruct array */
for(i=0;i<n;i++)
a[i]%=n;
getchar();
}
