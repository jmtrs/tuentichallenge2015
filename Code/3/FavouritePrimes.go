/* Problem 3 - Favourite Primes
 * Author: jmtrs
 * Languaje: GoLang
 * Execution: go run FavouritePrimes.go < a.in > a.out
 */
 
package main

import ( 
    "fmt"
    "bufio"
    //"io"
    "os"
    //"errors"
)

func read(fn string, num int) (string, error){
    
	f, err := os.Open(fn)
	if err != nil {
		return "", err
	}
	defer f.Close()
	bf := bufio.NewReader(f)
	var line string
		
	for i:=0; i<num; i++ {
		line, err = bf.ReadString('\n')
	}
	return line, nil
}

//func prime(line string)

func main() {
    
    var aux int
    var ini int
    var fin int
    
	fmt.Scanf("%d", &aux)
	
	for i := 1; i <= aux; i++ {
	    fmt.Scanf("%d %d", &ini, &fin)
	    if ini == 0{
		    for num:=ini; num<fin; num++{
		        if line, err := read("numbers.txt", num +1); err == nil {
		        	fmt.Println(line)
		        }
	    	}
	    }else if ini != 0{
	    	for num:=ini; num<fin; num++{
		        if line, err := read("numbers.txt", num); err == nil {
		        	fmt.Println(line)
		        	//prime(string(line))
		        }
	    	}
	    }
	}
}